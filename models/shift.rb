class Shift < Sequel::Model
	plugin :timestamps

	one_to_many :assignments
	many_to_many :users, :join_table => :assignments

	subset(:upcoming){(start_date >= Date.today) | (reminder_date >= Date.today)}
	subset(:past){(start_date < Date.today) & (reminder_date < Date.today)}
	subset(:active, :active => true)

	def validate
		super
		validates_presence [:start_date, :name]
		errors.add(:reminder_date, 'cannot be before start date') if reminder_date && (reminder_date < start_date)
		errors.add(:start_date, 'cannot be in the past') if start_date && new? && (start_date < Date.today)
		validates_integer :capacity, :allow_nil => true
	end
	
	def before_validation
		self.name ||= self.start_date.strftime("%A, %B %d")
		super
	end

	def activate
		update(:active => true)
	end

	def deactivate
		update(:active => false)
	end

	def self.mine(user) 
		where(:users => user)
	end

	def full?
		return false if self.capacity.blank?
		return (self.capacity <= self.assignments.count)
	end
end
