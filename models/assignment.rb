class Assignment < Sequel::Model
	plugin :timestamps
	plugin :enum

	#  				0			1			2		3		4
	enum :status, [:pending, :claimed, :assigned, :cancel, :missing]

	many_to_one :user
	many_to_one :shift

	def before_destroy
		cancel_action if status != :pending
	end

end
