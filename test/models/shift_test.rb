require File.expand_path(File.dirname(__FILE__) + '/../test_config.rb')

describe "Shift Model" do

	before(:each) do
		@shift = build(:shift)
	end

	it 'can construct a new instance' do
		@shift = Shift.new
		refute_nil @shift
	end

	describe "validations" do
		it "should automatically assign name if blank" do
			@shift.name = nil
			@shift.valid?
			@shift.name.must_equal @shift.start_date.strftime("%A, %B %d")
		end

		it "should require start date" do
			@shift.start_date = nil
			@shift.valid?.wont_equal true
			@shift.errors[:start_date].must_include "is not present"
		end

		it "should require that start date is in the future" do
			@shift.start_date = Date.today - 1
			@shift.valid?.wont_equal true
			@shift.errors[:start_date].wont_equal nil

			@shift.start_date = Date.today + 5
			@shift.valid?.must_equal true
		end

		it "should ensure that reminder is after start" do
			@shift = build(:shift, :reminder)
			@shift.reminder_date = Date.today - 7
			@shift.valid?.wont_equal true
			@shift.errors[:reminder_date].must_include "cannot be before start date"
		end

		it "valid object should save" do
			@shift = build(:shift)
			@shift.save.must_be_instance_of Shift
		end

	end
end
