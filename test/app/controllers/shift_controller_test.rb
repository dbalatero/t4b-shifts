require File.expand_path(File.dirname(__FILE__) + '/../../test_config.rb')

describe "/shifts" do
	
	describe "Normal user" do

		before do
			create_list(:shift, 10)
			@user = create(:user)
		end

		it "can list available shifts" do
			get "/shifts", nil , {'rack.session' => {:current_user => @user.id, :roles => []}}
			assert_includes last_response.body, "Sign up for Shifts"
			last_response.body.must_have_css("div.shift", :count => Shift.count)
		end

		it "without profile cannot see sign up" do
			get "/shifts", nil, {'rack.session' => {:current_user => @user.id, :roles => []}}
			last_response.body.must_have_css("div.alert", :text => "You can't sign up for shifts without completing your profile!")
		end

		it "can sign up for a shift" do
			post "/shifts/signup/#{Shift.first.id}", {:amount => 2, :availability => "only after 5pm"}, {'rack.session' => {:current_user => @user.id, :roles => []}}
			last_response.status.must_equal 302
			assert_includes Shift.first.users, @user
		end

		it "cannot sign up for a full shift" do
			@shift = create(:shift, :capacity => 1)
			post "/shifts/signup/#{@shift.id}", {:amount => 2}, {'rack.session' => {:current_user => @user.id, :roles => []}}
			last_response.status.must_equal 302
			assert_includes @shift.users, @user
			# Sign up for 2nd should fail
			post "/shifts/signup/#{@shift.id}", {:amount => 2}, {'rack.session' => {:current_user => @user.id, :roles => []}}
			last_response.status.must_equal 302
			assert_includes last_request.env['rack.session'][:_flash][:error], "full"
		end

		it "can view shifts that they have signed up for" do
			Shift.first.add_user(@user)
			get "/shifts", nil, {'rack.session' => {:current_user => @user.id, :roles => []}}
			last_response.body.must_have_css("#mine div.shift", :count => Shift.mine(@user).count)
		end

		it "can forfeit a shift" do
			get "/shifts/forfeit/#{Shift.first.id}", nil , {'rack.session' => {:current_user => @user.id, :roles => []}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
			last_response.status.must_equal 200
			refute_includes Shift.first.users, @user
		end

		it "can view shift details" do
			@shift = Shift.first
			get "/shifts/view/#{@shift.id}", nil, {'rack.session' => {:current_user => @user.id, :roles => []}}
			last_response.body.must_have_css("h1", :text => @shift.name)
			last_response.body.must_have_content(@shift.start_date.strftime("%A, %B %d"))
		end

		it "can view the feed" do
			@shift = Shift.first
			get "/shifts/feed"
			last_response.body.must_have_xpath('//item//title', :text => @shift.name)
		end

		it "doesn't break if a shift lacks description" do
			@shift = Shift.first
			@shift.details = nil
			@shift.save
			get "/shifts/feed"
			last_response.body.must_have_xpath('//item//title', :text => @shift.name)
		end

	end

	describe "Admin user" do

		before do
			@user = create(:user)
		end

		describe "can create shifts" do
			
			it "should display form" do
				get "/shifts/new", nil, {'rack.session' => {:current_user => @user.id, :roles => ["admin"]}}
				last_response.body.must_have_field('name')
				last_response.body.must_have_field('start_date')
				last_response.body.must_have_field('reminder_date')
				last_response.body.must_have_field('details', :type => "textarea")
				last_response.body.must_have_field('capacity', :type => "number")
				last_response.body.must_have_field('event_link', :type => "url")
			end
			
			describe "should accept input" do
				
				before do
					@params = attributes_for(:shift, :reminder)
				end

				it "should save valid input" do
					start = Shift.count
					post "/shifts/create", {:form => @params}, {'rack.session' => {:current_user => @user.id, :roles => ["admin"]}}
					assert_equal last_request.env['rack.session'][:_flash][:info], "Created shift!"
					assert_equal Shift.count, start+1
				end

				it "should redirect with input on error" do
					@params[:start_date] = Date.today - 1
					post "/shifts/create", {:form => @params}, {'rack.session' => {:current_user => @user.id, :roles => ["admin"]}}
					assert last_response.redirect?
				end
			end

			describe "should validate and display errors" do

				before do
					@params = attributes_for(:shift, :reminder)
					@start = Shift.count
				end
				
				it "should validate date" do
					@params[:start_date] = Date.today - 1
					post "/shifts/create", {:form => @params}, {'rack.session' => {:current_user => @user.id, :roles => ["admin"]}}
					Shift.count.wont_equal @start+1
					last_request.env['rack.session'][:_flash][:error].must_include "Start date"
				end

				it "should validate reminder" do
					@params[:start_date] = Date.today
					@params[:reminder_date] = Date.today - 1
					post "/shifts/create", {:form => @params}, {'rack.session' => {:current_user => @user.id, :roles => ["admin"]}}
					Shift.count.wont_equal @start+1
					last_request.env['rack.session'][:_flash][:error].must_include "Reminder date"
				end

			end

		end

		describe "can edit shifts" do

			before do
				@shift = create(:shift, :reminder)
			end

			it "shows the form" do
				get "/shifts/edit/#{@shift.id}", nil, {'rack.session' => {:current_user => @user.id, :roles => ["admin"]}}
				last_response.body.must_have_field('name', :with => @shift.name)
				last_response.body.must_have_field('start_date', :with => @shift.start_date)
				last_response.body.must_have_field('reminder_date', :with => @shift.reminder_date)
				last_response.body.must_have_field('details', :type => "textarea", :with => @shift.details)
				last_response.body.must_have_field('capacity', :type => "number", :with => @shift.capacity)
				last_response.body.must_have_field('event_link', :type => "url")
			end

			it "saves changes" do
				put "/shifts/update/#{@shift.id}", {:form => {:name => "Something else!", :start_date => Date.today.strftime("%Y/%m/%d")}}, {'rack.session' => {:current_user => @user.id, :roles => ["admin"]}}
				assert_equal Shift[@shift.id].name, "Something else!"
				assert_equal Shift[@shift.id].start_date, Date.today
			end
		end
		
		it "can delete shifts" do
			@shift = create(:shift, :reminder)
			start = Shift.count
			delete "/shifts/destroy/#{@shift.id}", nil, {'rack.session' => {:current_user => @user.id, :roles => ["admin"]}, "HTTP_X_REQUESTED_WITH" => "XMLHttpRequest"}
			assert_equal Shift.count, start - 1
		end

		it "can view users signed up for shifts" do
			create(:user, :with_shifts)
			@user = create(:user, :admin)
			get "/shifts/view/#{Shift.last.id}", nil, {'rack.session' => {:current_user => @user.id, :roles => ["admin"]}}
			last_response.body.must_have_css('h4', :text => "Assign Users to Shift")
			last_response.body.must_have_css('.datatable tbody')
		end
	
	end
	
end
