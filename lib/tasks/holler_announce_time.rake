namespace :holler do
  
  desc 'Announces the time for starting and stopping texts'
  task :announce, [:message] => :environment do |t, args|
    args.with_defaults(:message => 'Test')

    Textshifts::App::AssignmentHelper::hollerAnnounce(args[:message])
  end
end

