module Textshifts
	class App < Padrino::Application
		register ScssInitializer
		register Padrino::Mailer
		register Padrino::Helpers

		require 'json'
		require 'oj'

		##
		# Caching support.
		#
		# register Padrino::Cache
		# enable :caching
		#
		# You can customize caching store engines:
		#
		# set :cache, Padrino::Cache.new(:LRUHash) # Keeps cached values in memory
		# set :cache, Padrino::Cache.new(:Memcached) # Uses default server at localhost
		# set :cache, Padrino::Cache.new(:Memcached, :server => '127.0.0.1:11211', :exception_retry_limit => 1)
		# set :cache, Padrino::Cache.new(:Memcached, :backend => memcached_or_dalli_instance)
		# set :cache, Padrino::Cache.new(:Redis) # Uses default server at localhost
		# set :cache, Padrino::Cache.new(:Redis, :host => '127.0.0.1', :port => 6379, :db => 0)
		# set :cache, Padrino::Cache.new(:Redis, :backend => redis_instance)
		# set :cache, Padrino::Cache.new(:Mongo) # Uses default server at localhost
		# set :cache, Padrino::Cache.new(:Mongo, :backend => mongo_client_instance)
		# set :cache, Padrino::Cache.new(:File, :dir => Padrino.root('tmp', app_name.to_s, 'cache')) # default choice
		#

		##
		# Application configuration options.
		#
		# set :raise_errors, true       # Raise exceptions (will stop application) (default for test)
		# set :dump_errors, true        # Exception backtraces are written to STDERR (default for production/development)
		# set :show_exceptions, true    # Shows a stack trace in browser (default for development)
		# set :logging, true            # Logging in STDOUT for development and file for production (default only for development)
		# set :public_folder, 'foo/bar' # Location for static assets (default root/public)
		# set :reload, false            # Reload application files (default in development)
		# set :default_builder, 'foo'   # Set a custom form builder (default 'StandardFormBuilder')
		# set :locale_path, 'bar'       # Set path for I18n translations (default your_apps_root_path/locale)
		# disable :sessions             # Disabled sessions by default (enable if needed)
		# disable :flash                # Disables sinatra-flash (enabled by default if Sinatra::Flash is defined)
		# layout  :my_layout            # Layout can be in views/layouts/foo.ext or views/foo.ext (default :application)
		#

		configure :development do
			disable :asset_stamp # no asset timestamping for dev
			OmniAuth.config.on_failure = Proc.new { |env|
				OmniAuth::FailureEndpoint.new(env).redirect_to_failure
			}
		end

		before do
			if current_user && (current_user.phone.blank? || current_user.device.blank? || current_user.tz.blank?)
				flash.now[:danger] = "You can't sign up for shifts without completing your profile! <a href='#{url_for(:users, :edit)}' class='alert-link'>Click here</a> to finish setting up your profile!"
			end
		end

		get "/" do
			slim :index
		end

		get :login, :map => "/login" do
			redirect url_for(:shifts, :index), :info => "You're already logged in!" if current_user
			redirect url_for("/auth/slack")
		end

		error 404 do
			render 'errors/404'
		end

		error 403 do
			render 'errors/403'
		end

		error 500 do
			render 'errors/500'
		end
	end

	class Sequel::Model
		alias_method :save!, :save
	end
end
