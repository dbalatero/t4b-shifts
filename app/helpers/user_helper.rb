# Helper methods defined here can be accessed in any controller or view in the application

module Textshifts
  class App
    module UserHelper
      def timezone_options
      	tz_select = []
		TZInfo::Country.get("US").zones.each do |z|
			name = z.current_period.abbreviation.to_s
			offset = z.current_period.utc_offset / 3600
			tz_select << ["#{z.name} #{name} (#{offset}:00)", name]
		end
		tz_select
      end
    end

    helpers UserHelper
  end
end
