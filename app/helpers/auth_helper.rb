# Helper methods defined here can be accessed in any controller or view in the application

module Textshifts
  class App
    module AuthHelper
    	def current_user=(user)
			@current_user = user
		end

		def current_user?(user)
			@current_user == user
		end

		def current_user
			@current_user ||= User.first_by_id(session[:current_user])
		end

		def sign_in(user)
			# Set session
			session[:current_user] = user.id
			session[:roles] = user.roles
			
			# update_last_logged_in_time(user)
			self.current_user = user
			@current_user = user
		end

		def sign_out
			session.clear
		end

		def logged_in?
			!current_user.blank?
		end

		def authorized?(allowed_roles)
			return false unless session[:current_user]
			return true unless allowed_roles
			return false unless session[:roles]
			roles = session[:roles]
			roles.any? { |r| allowed_roles.include? r }
		end

		def authorize!(allowed_roles=nil)
			if logged_in?
				halt 403 unless authorized?(allowed_roles)
			else
				redirect url_for("/login?return_url=#{request.env['PATH_INFO']}"), info: "You need to be logged in to access this." unless authorized?(allowed_roles)
			end
		end

		def in_channel(token, channel = ENV['TEXT_TEAM_ID'])
			require 'httparty'
			response = HTTParty.get("https://slack.com/api/groups.list?token=#{token}&exclude_archived=1")
			return !!response["groups"].find {|h1| h1["id"] == channel}
		end

    end

    helpers AuthHelper
  end
end
