Textshifts::App.controllers :auth do

	get :slack_callback, :map => "/auth/slack/callback" do
		omniauth = request.env["omniauth.auth"]

		if in_channel(omniauth[:credentials][:token], ENV['TEXT_TEAM_ID'])

			@user = User.first_by_uid(omniauth[:info][:user]) || User.new_from_auth(omniauth[:info])

			if params[:return_url].blank?
				url = "/shifts" # main shift screen
			else
				url = params[:return_url]
			end

			if @user.errors.empty? && @user.exists?
				if @user.active
					sign_in(@user)
					redirect url_for(url), success: "You are logged in as #{@user.uid}"
				else
					redirect url_for('/'), error: "Your account has been deactivated, please contact an administrator."
				end
			else
				redirect url_for('/'), error: "Could not log you in for the following reasons: #{@user.errors.full_messages.join(', ').humanize}"
			end
		else
			redirect url_for("/"), error: "You must be a member of the #textforbernie slack channel before you can log in to request shifts! Make sure that you have <a href='https://docs.google.com/document/d/1i8rDMDI5cRTGj2L6aT8gdp5s4DO8xslbuzIJD0mNlZc/pub' class='alert-link'>read our FAQ</a> and joined the slack channel first!"
		end

	end

	get :slack_callback_failed, :map => "/auth/failure" do
		flash[:error] = "Error logging in with Slack: #{params[:message].humanize}"
		redirect url("/")
	end

	get :destroy, :map => '/logout' do
		sign_out
		redirect url_for('/'), info: "Logged Out"
	end

end
