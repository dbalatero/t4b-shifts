Sequel.migration do
  up do
    create_table :assignments do
      primary_key :id
      foreign_key :user_id, :users, :on_delete => :cascade
      foreign_key :shift_id, :shifts, :on_delete => :cascade
      Integer :status, :size => 1
      String :notes, :text => true

      DateTime :created_at
      DateTime :updated_at

      index [:user_id, :shift_id]
    end
  end

  down do
    drop_table :assignments
  end
end
