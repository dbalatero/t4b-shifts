Sequel.migration do
  up do
  	alter_table :users do
  		add_column :location, String
  	end
  end

  down do
  	alter_table :users do
  		drop_column :location
  	end
  end
end