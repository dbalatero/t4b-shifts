Sequel.migration do
  up do
  	alter_table(:users) do
  		add_column :tz, String, :size => 4
  	end
  end

  down do
  	alter_table(:users) do
  		drop_column :tz
  	end
  end
end
