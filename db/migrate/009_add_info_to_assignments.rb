Sequel.migration do
  up do
    alter_table :assignments do
      add_column :availability, String
    end
  end

  down do
    alter_table :assignments do
      drop_column :availability
    end
  end
end
