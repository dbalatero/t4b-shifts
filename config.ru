#!/usr/bin/env rackup
# encoding: utf-8

# This file can be used to start Padrino,
# just execute it from the command line.

require File.expand_path("../config/boot.rb", __FILE__)

use Rack::Session::Cookie,
	:expire_after => 3600 * 24,
	:path => '/',
	:secret => ENV['SESSION_SECRET']

use Rack::Protection
# use Rack::Protection::FormToken, :origin_whitelist => "localhost"

use OmniAuth::Builder do
	provider :slack,
		ENV["SLACK_APP_ID"],
		ENV["SLACK_APP_KEY"],
		scope: ENV["SLACK_SCOPES"],
		team: ENV["SLACK_TEAM_ID"]
end

run Padrino.application