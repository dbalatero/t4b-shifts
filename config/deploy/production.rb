server 'textforbernie.com', user: 'hustlebern', roles: %w{app db web}
set :deploy_to, '/home/hustlebern/textshifts'
set :linked_files, %w{.env.production .htaccess}

set :ssh_options, {
    keys: %w(/home/hustlebern/.ssh/id_rsa),
    forward_agent: false,
}