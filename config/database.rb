Sequel::Model.plugin :schema
Sequel::Model.plugin :timestamps
Sequel::Model.plugin :validation_helpers
Sequel::Model.plugin :json_serializer
Sequel::Model.plugin :force_encoding, 'UTF-8'
Sequel::Model.raise_on_save_failure = false # Do not throw exceptions on failure

Sequel::Model.db = case Padrino.env
	# If you would prefer to use sqlite for development, uncomment the following line
	# when :development	then Sequel.connect("sqlite://development.db", :loggers => [logger])
	when :development	then Sequel.connect({
			:host => ENV['DATABASE_HOST'],
			:database => ENV['DATABASE_DB'],
			:user => ENV['DATABASE_USER'],
			:password => ENV['DATABASE_PASS'],
			:adapter => 'mysql2',
			:encoding => 'utf8mb4'
		}, :loggers => [logger])
	when :staging		then Sequel.connect(ENV['DATABASE_URL'], :loggers => [logger])
	when :production	then Sequel.connect({
			:host => ENV['DATABASE_HOST'],
			:database => ENV['DATABASE_DB'],
			:user => ENV['DATABASE_USER'],
			:password => ENV['DATABASE_PASS'],
			:adapter => 'mysql2',
			:encoding => 'utf8mb4'
		}, :loggers => [logger])
	when :test			then Sequel.connect({
			:host => ENV['DATABASE_HOST'],
			:database => 'hustletest',
			:user => ENV['DATABASE_USER'],
			:password => ENV['DATABASE_PASS'],
			:adapter => 'mysql2',
			:encoding => 'utf8mb4'
		}, :loggers => [logger])
end
