# Use this file to easily define all of your cron jobs.
#
# It's helpful, but not entirely necessary to understand cron before proceeding.
# http://en.wikipedia.org/wiki/Cron

# Example:
#
set :output, "log/cron_log.log"

job_type :padrino_rake, 'cd :path && padrino rake :task -e :environment'

# Server is in Pacific Time
every 1.day, :at => '5:59am' do
	padrino_rake "holler:announce[\"It is now 9AM in the *EASTERN* Time Zone. You may now begin sending and reply to messages in the *EASTERN* Time Zone\"]"
end
every 1.day, :at => '6:59am' do
	padrino_rake "holler:announce[\"It is now 9AM in the *CENTRAL* Time Zone. You may now begin sending and reply to messages in the *CENTRAL* Time Zone\"]"
end
every 1.day, :at => '7:59am' do
	padrino_rake "holler:announce[\"It is now 9AM in the *MOUNTAIN* Time Zone. You may now begin sending and reply to messages in the *MOUNTAIN* Time Zone\"]"
end
every 1.day, :at => '8:59am' do
	padrino_rake "holler:announce[\"It is now 9AM in the *PACIFIC* Time Zone. You may now begin sending and reply to messages in the *PACIFIC* Time Zone\"]"
end

every 1.day, :at => '5:59pm' do
	padrino_rake "holler:announce[\"It is now 9PM in the *EASTERN* Time Zone. Please stop sending messages to recipients in the *EASTERN* Time Zone. If you receive a reply you will need to respond after 9am local time. If you're unsure of the time zone in a specific location you can google for _Time in location_. Don't get Bernie in trouble!\"]"
end
every 1.day, :at => '6:59pm' do
	padrino_rake "holler:announce[\"It is now 9PM in the *CENTRAL* Time Zone. Please stop sending messages to recipients in the *CENTRAL* Time Zone. If you receive a reply you will need to respond after 9am local time. If you're unsure of the time zone in a specific location you can google for _Time in location_. Don't get Bernie in trouble!\"]"
end
every 1.day, :at => '7:59pm' do
	padrino_rake "holler:announce[\"It is now 9PM in the *MOUNTAIN* Time Zone. Please stop sending messages to recipients in the *MOUNTAIN* Time Zone. If you receive a reply you will need to respond after 9am local time. If you're unsure of the time zone in a specific location you can google for _Time in location_. Don't get Bernie in trouble!\"]"
end
every 1.day, :at => '8:59pm' do
	padrino_rake "holler:announce[\"It is now 9PM in the *PACIFIC* Time Zone. Please stop sending messages to recipients in the *PACIFIC* Time Zone. If you receive a reply you will need to respond after 9am local time. If you're unsure of the time zone in a specific location you can google for _Time in location_. Don't get Bernie in trouble!\"]"
end
